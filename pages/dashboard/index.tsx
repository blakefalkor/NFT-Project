export default function Dashboard() {

  return (
    <>
      <div className="three-columns h-full">
        <div className="">
          <h2 className="text-2xl font-bold text-zinc-900 capitalize">Mint your NFT</h2>
          <div className=" items-center">
            <img className=" h-auto" src="https://i.pinimg.com/originals/ac/3c/5a/ac3c5ae3d80f8a7449a252dd72d551a5.gif" alt="" />
            <h3>In your Account: 0</h3>
            <button className="inline-flex bg-zinc-500 justify-center hover:bg-zinc-700 text-white font-bold py-3 px-5 rounded items-center text-center">Mint NFT</button>
          </div>
        </div>
        <div className="">
          <h2 className="text-2xl font-bold text-zinc-900 capitalize">Stake your NFT</h2>
          <div className=" items-center">
            <img className=" h-auto" src="https://i.pinimg.com/originals/ac/3c/5a/ac3c5ae3d80f8a7449a252dd72d551a5.gif" alt="" />
            <h3>In your Account: 0</h3>
            <button className="inline-flex bg-zinc-500 justify-center hover:bg-zinc-700 text-white font-bold py-3 px-5 rounded items-center text-center">Mint NFT</button>
          </div>
        </div>
        <div className="">
          <h2 className="text-2xl font-bold text-zinc-900 capitalize">Mint your NFT</h2>
          <div className=" items-center">
            <img className=" h-auto" src="https://i.pinimg.com/originals/ac/3c/5a/ac3c5ae3d80f8a7449a252dd72d551a5.gif" alt="" />
            <h3>In your Account: 0</h3>
            <button className="inline-flex bg-zinc-500 justify-center hover:bg-zinc-700 text-white font-bold py-3 px-5 rounded items-center text-center">Mint NFT</button>
          </div>
        </div>
      </div>
    </>
  );
}
